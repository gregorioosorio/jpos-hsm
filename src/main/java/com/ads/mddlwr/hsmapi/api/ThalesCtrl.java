package com.ads.mddlwr.hsmapi.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jpos.iso.FSDISOMsg;
import org.jpos.iso.ISODate;
import org.jpos.iso.ISOUtil;
import org.jpos.q2.iso.QMUX;
import org.jpos.q2.iso.ThalesFSDISOMsg;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.space.SpaceUtil;
import org.jpos.util.FSDMsg;
import org.jpos.util.NameRegistrar;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.Map;

@RestController
@CrossOrigin
public class ThalesCtrl {

    private static final String KEYCHANGE = "CAMS_HSM.";

    @PostMapping(value = "/api/thales", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Mono<Map<String, String>> webiso(@RequestBody String json) throws Exception {
        Space sp = SpaceFactory.getSpace();
        long traceNumber = SpaceUtil.nextLong(sp, KEYCHANGE) % 1000000;
        FSDMsg txm = new FSDMsg("file:cfg/hsm-");
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map;
        map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});

        txm.set("41", ISODate.getDateTime(new Date()));
        txm.set("11", ISOUtil.zeropad(traceNumber, 18));

        for (Map.Entry<String, String> entry : map.entrySet())
        {
            if(!entry.getKey().equalsIgnoreCase("mux")) {
                txm.set(entry.getKey(), entry.getValue());
            }
        }
        txm.setCharset(ISOUtil.CHARSET);

        ThalesFSDISOMsg tx = new ThalesFSDISOMsg(txm);

        QMUX out = (QMUX) NameRegistrar.get("mux."+map.get("mux"));
        FSDISOMsg response = (FSDISOMsg) out.request(tx, 10000);

        if (response != null) {
            return Mono.just(response.getFSDMsg().getMap());
        } else {
            return null;
        }

    }
}
